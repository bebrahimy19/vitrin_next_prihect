export function getRandomInt(max) {
  return Math.ceil(Math.random() * max);
}

export function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

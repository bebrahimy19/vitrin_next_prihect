module.exports = {
  reactStrictMode: false,
  images: {
    domains: [
      "vitrin.me",
      "co0kie.github.io",
      "fakestoreapi.com",
      "tailwindui.com",
      "www.pngmart.com",
    ],
  },
};

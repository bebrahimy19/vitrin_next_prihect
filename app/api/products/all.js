import AppHttp from "../AppHttp";

const getAllProducts = async() => {
    const http = await AppHttp();
    const { data } = await http.get(`/products`);
    return data;
};

export { getAllProducts };
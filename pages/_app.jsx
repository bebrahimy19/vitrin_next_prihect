import "../styles/globals.css";

import NProgress from "nprogress";
import "nprogress/nprogress.css";
import Router from "next/router";
import Layouts from "../components/Layouts";
import React from "react";

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

function MyApp({ Component, pageProps }) {
  return (
    <React.StrictMode>
      <Layouts>
        <Component {...pageProps} />
      </Layouts>
    </React.StrictMode>
  );
}

export default MyApp;

import Head from "next/head";
import React from "react";
import { getProduct } from "../../app/api/products/product";
import ProductItem from "../../components/Product";

export default function Product({ productData }) {
  return (
    <>
      <Head>
        <meta charset="utf-8" />
        <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1 , maximum-scale=1.0, user-scalable=no"
        />

        <link
          rel="preload"
          href="https://hs3-cf.behtarino.com/static/fonts/Dana/dana-extralight.woff2"
          type="font/woff2"
          as="font"
        />
        <link rel="icon" href="/images/favicon.png" />
        <link rel="canonical" href="https://vitrin.me/" />
        <script async src="https://d.clarity.ms/s/0.6.32/clarity.js"></script>
        <script async></script>
        <title>{productData.title}</title>
        <meta name="description" content={productData.description} />
      </Head>
      <ProductItem productData={productData} />
    </>
  );
}

export async function getServerSideProps(context) {
  const { params } = context;
  const { id } = params;

  let data = await getProduct(id);
  return {
    props: { productData: data },
  };
}

import React from "react";
import { getAllProducts } from "../../app/api/products/all";
import ProductsList from "../../components/ProductsList";

export default function Products({ productsData }) {
  return (
    <>
      <ProductsList productsData={productsData} />
    </>
  );
}

export const getStaticProps = async () => {
  let data = await getAllProducts();

  return {
    props: { productsData: data },
  };
};

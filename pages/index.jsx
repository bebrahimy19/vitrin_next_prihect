import Head from "next/head";
import HomePage from "../components/HomePage/HomePage";

export default function Home({}) {
  return (
    <div>
      <Head>
        <meta charset="utf-8" />
        <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1 , maximum-scale=1.0, user-scalable=no"
        />

        <link
          rel="preload"
          href="https://hs3-cf.behtarino.com/static/fonts/Dana/dana-extralight.woff2"
          type="font/woff2"
          as="font"
        />
        <link rel="icon" href="/images/favicon.png" />
        <link rel="canonical" href="https://vitrin.me/" />
        <script async src="https://d.clarity.ms/s/0.6.32/clarity.js"></script>
        <script async></script>
        <title>طراحی سایت | تا 200% فروش بیشتر با ویترین | Vitrin</title>
        <meta name="description" content="Generated by create next app" />
      </Head>
      <HomePage />
    </div>
  );
}

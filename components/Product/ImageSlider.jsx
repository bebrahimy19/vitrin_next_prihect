import Image from "next/image";
import { useEffect, useState } from "react";
import { classNames } from "../../functions";

const imagesList = [
  {
    id: 0,
    imageSrc:
      "https://www.pngmart.com/files/1/Nike-Shoes-Transparent-Background.png",
  },
  {
    id: 1,
    imageSrc: "https://www.pngmart.com/files/1/Nike-Shoes-PNG-Image.png",
  },
  {
    id: 2,
    imageSrc:
      "https://www.pngmart.com/files/1/Nike-Shoes-Transparent-Background.png",
  },
  {
    id: 3,
    imageSrc: "https://www.pngmart.com/files/1/Nike-Shoes-PNG-Image.png",
  },
  {
    id: 4,
    imageSrc:
      "https://www.pngmart.com/files/1/Nike-Shoes-Transparent-Background.png",
  },
  {
    id: 5,
    imageSrc: "https://www.pngmart.com/files/1/Nike-Shoes-PNG-Image.png",
  },
];

export default function ImageSlider() {
  const [selectedImage, setSelectedImage] = useState(0);
  const ImagesSliderLength = imagesList.length;

  useEffect(() => {
    let slider = setInterval(() => {
      setSelectedImage(
        selectedImage < ImagesSliderLength - 1 ? selectedImage + 1 : 0
      );
    }, 8000);
    return () => clearInterval(slider);
  }, [selectedImage]);

  return (
    <div className="relative shadow-ImageBox lg:col-span-1 h-full flex flex-col-reverse justify-end items-center bg-gradient-to-b from-lightGreen to-darkGreen rounded-t-lg lg:rounded-tr-none lg:rounded-l-lg px-3 sm:px-6 py-4 sm:py-8">
      {/* Image selector */}

      <div className="absolute bottom-4 lg:bottom-12 flex">
        {imagesList.map((item) => (
          <div
            key={item.id}
            onClick={() => setSelectedImage(item.id)}
            className={classNames(
              item.id === selectedImage ? "bg-white" : "bg-sliderDarkBg",
              "w-3 h-3 mx-1 shadow rounded-full cursor-pointer"
            )}
          ></div>
        ))}
      </div>
      <div className="w-full flex flex-col justify-center lg:block my-6">
        <div className="flex justify-center">
          <Image
            width={50}
            height={20}
            src={
              "http://co0kie.github.io/codepen/nike-product-page/nikeLogo.png"
            }
            alt="nike-logo"
          />
        </div>
        <div className="flex justify-center">
          <div className="w-72 h-auto my-12">
            <div className="lg:absolute lg:-left-10 xl:-left-12 w-full object-center object-cover">
              <Image
                width={450}
                height={380}
                src={imagesList[selectedImage].imageSrc}
                alt="nike-shoes"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

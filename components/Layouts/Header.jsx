import Link from "next/link";
import { getRandomInt } from "../../functions";
import VitrinLogoSvg from "./VitrinLogoSvg";

const navigation = [
  { id: 1, name: "Products", href: "/products" },
  { id: 2, name: "Random Product", href: "/products" },
  // {id: 3,  name: "Docs", href: "#" },
  // {id: 4,  name: "Company", href: "#" },
];

export default function Header() {
  let max = 20;

  return (
    <header className="bg-white">
      <nav
        className="inset-0 max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 border-b"
        aria-label="Top"
      >
        <div className="w-full py-6 flex items-center justify-between border-b border-indigo-500 lg:border-none">
          <div className="flex items-center">
            <Link href="/">
              <a className="">
                <span className="sr-only">Vitrin</span>
                <VitrinLogoSvg />
              </a>
            </Link>

            <div className="hidden ml-10 space-x-8 lg:block">
              {navigation.map((link) => (
                <Link
                  key={link.name}
                  href={
                    link.id === 2
                      ? `${link.href}/${getRandomInt(max)}`
                      : link.href
                  }
                >
                  <a className="text-base font-medium text-gray-500 hover:text-gray-400">
                    {link.name}
                  </a>
                </Link>
              ))}
            </div>
          </div>
          <div className="ml-10 space-x-4">
            <Link href="/">
              <a className="inline-block bg-white py-2 px-4 border border-transparent rounded-md text-base font-medium text-indigo-600 hover:bg-indigo-50">
                Sign in
              </a>
            </Link>
            <Link href="/">
              <a className="inline-block bg-indigo-500 py-2 px-4 border border-transparent rounded-md text-base font-medium text-white hover:bg-opacity-75">
                Sign up
              </a>
            </Link>
          </div>
        </div>
        {/* <div className="py-4 flex flex-wrap justify-center space-x-6 lg:hidden">
          {navigation.map((link) => (
            <Link key={link.name} href={link.href}>
              <a className="text-base font-medium text-white hover:text-indigo-50">
                {link.name}
              </a>
            </Link>
          ))}
        </div> */}
      </nav>
    </header>
  );
}
